<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $edad
 * @property string|null $poblacion
 * @property string|null $telefono
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['edad'], 'integer'],
            [['nombre', 'poblacion'], 'string', 'max' => 50],
            [['apellidos'], 'string', 'max' => 100],
            [['telefono'], 'string', 'max' => 12],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'edad' => 'Edad',
            'poblacion' => 'Poblacion',
            'telefono' => 'Telefono',
        ];
    }
}
